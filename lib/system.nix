{
  user,
  inputs,
  ...
}: let
  username = "ap";
  homeDirectory = "/home/${username}";
  inherit (inputs.nixpkgs) lib;
in {
  mkHost = {
    extraModules,
    systemConfig,
  }:
    lib.nixosSystem {
      specialArgs = {
        inherit inputs username homeDirectory;
        sysPersistDir = "/persist";
      };

      modules =
        [
          {
            imports = [
              ../hosts/${systemConfig.core.hostname}
              ../modules/system
            ];

            ap = systemConfig;

            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              extraSpecialArgs = {
                inherit inputs username homeDirectory;
              };
              users.${username} = user.mkConfig;
            };
          }

          inputs.home-manager.nixosModule
          inputs.impermanence.nixosModule
          inputs.agenix.nixosModules.default
          inputs.stylix.nixosModules.stylix
        ]
        ++ extraModules;
    };
}
