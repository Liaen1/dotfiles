let
  t480 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK60OtZ/aLolwVKFalkZUBwUU6OIffI8raxKzMXDCYpO";
in {
  "password.age".publicKeys = [t480];
}
