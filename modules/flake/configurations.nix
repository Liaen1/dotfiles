{inputs, ...}: let
  utils = import ../../lib {inherit inputs;};
  inherit (utils.system) mkHost;
in {
  flake.nixosConfigurations = {
    t480 = mkHost {
      extraModules = [inputs.nixos-hardware.nixosModules.lenovo-thinkpad-t480 {hardware.opengl.enable = true;}];
      systemConfig = {
        core.hostname = "t480";
        hardware = {
          bluetooth.enable = true;
        };
        services = {
          mullvad.enable = true;
        };
      };
    };
  };
}
