{
  lib,
  config,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.ap.hardware.bluetooth;
in {
  options.ap.hardware.bluetooth.enable = mkEnableOption "Enable Bluetooth";
  config = mkIf cfg.enable {
    hardware.bluetooth = {
      enable = true;
      powerOnBoot = false;
    };
  };
}
