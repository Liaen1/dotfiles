{lib, ...}: let
  inherit (lib) mkOption types;
in {
  options.ap.core.hostname = mkOption {
    description = "System hostname";
    type = types.str;
  };
}
