{pkgs, ...}: {
  stylix = {
    image = ../../../images/wallpaper.jpg;
    base16Scheme = "${pkgs.base16-schemes}/share/themes/gruvbox-dark-medium.yaml";
    polarity = "dark";
  };
}
