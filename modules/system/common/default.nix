{
  inputs,
  sysPersistDir,
  ...
}: {
  imports = [
    ./user.nix
    ./networking.nix
    ./sound.nix
    ./theme.nix

    ./fonts.nix
    ./greetd.nix
    ./i18n.nix
    ./nix.nix
    ./man.nix
    ./xdg.nix
  ];

  environment = {
    persistence."${sysPersistDir}" = {
      directories = [
        "/etc/NetworkManager"
        "/var/cache"
        "/var/lib"
        "/var/log"
      ];
      files = [
        "/etc/nix/id_rsa"
      ];
    };

    # Required for systemd journal
    etc."machine-id".source = "${sysPersistDir}/etc/machine-id";
  };

  nixpkgs = {
    overlays = [inputs.nur.overlay];
    config.allowUnfree = true;
  };

  # Required for impermanence/home-manager
  programs.fuse.userAllowOther = true;

  # Install age cli tool
  environment.systemPackages = [inputs.agenix.packages.x86_64-linux.default];
  # Tell age where the keys are
  age.identityPaths = ["${sysPersistDir}/etc/ssh/ssh_host_ed25519_key"];

  programs.git = {
    enable = true;
    config.user = {
      name = "Aatos Piha";
      email = "aat.pih@protonmail.com";
    };
  };

  security.polkit.enable = true;

  # Allow swaylock to unlock the computer for us
  security.pam.services.swaylock.text = "auth include login";
}
