{
  pkgs,
  username,
  inputs,
  ...
}: let
  wm = "${inputs.hyprland.packages.${pkgs.system}.default}/bin/Hyprland";
in {
  services.greetd = {
    enable = true;
    settings = {
      initial_session = {
        command = wm;
        user = username;
      };
      default_session = {
        command = "${pkgs.greetd.tuigreet}/bin/tuigreet --cmd ${wm} --time --time-format %Y-%m-%d %H:%M:%S% --remember";
        user = username;
      };
    };
  };
}
