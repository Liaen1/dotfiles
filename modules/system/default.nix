{...}: {
  imports = [
    ./common
    ./core
    ./hardware
    ./services
  ];
}
