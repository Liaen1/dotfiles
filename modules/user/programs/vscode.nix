{pkgs, ...}: {
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions; [
      bbenoist.nix
      github.copilot
      jdinhlife.gruvbox
      kamadorueda.alejandra
    ];
    userSettings = {
      "editor.inlineSuggest.enabled" = true;
      "editor.fontLigatures" = true;

      "security.workspace.trust.untrustedFiles" = "open";

      "window.menuBarVisibility" = "hidden";
      "window.zoomLevel" = 2;
      "workbench.startupEditor" = "none";
      "workbench.colorTheme" = "Gruvbox Dark Medium";
    };
  };

  # Tell VS Code to use Wayland
  home.file.".vscode-oss/argv.json" = {
    recursive = true;
    text = ''
      {
        "enable-features": "UseOzonePlatform",
        "ozone-platform": "wayland",
        "password-store": "gnome"
      }
    '';
  };

  stylix.targets.vscode.enable = false;
}
