{pkgs, ...}: {
  imports = [
    ./firefox
    ./mpv.nix
    ./sioyek.nix
    ./speedcrunch.nix
    ./vscode.nix
  ];

  home.packages = with pkgs; [
    # utilities
    pavucontrol

    # media
    gimp

    # chat
    signal-desktop
    webcord

    # other
    qbittorrent
    ungoogled-chromium
  ];

  programs = {
    imv.enable = true;
  };
}
