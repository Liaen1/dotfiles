{
  programs.sioyek = {
    enable = true;
    bindings = {
      "toggle_dark_mode" = "i";
    };
  };

  xdg.mimeApps.defaultApplications."application/pdf" = "sioyek.desktop";
}
