{
  osConfig,
  inputs,
  pkgs,
  username,
  homeDirectory,
  ...
}: {
  imports = [
    ./tools.nix
    (import ./ssh.nix {inherit homeDirectory;})
    ./starship.nix
  ];

  home.packages = with pkgs; [fishPlugins.fzf-fish fishPlugins.grc fishPlugins.pisces grc];

  programs.fish = {
    enable = true;

    plugins = [
      {
        name = "fish-ssh-agent";
        src = inputs.fish-ssh-agent;
      }
    ];

    shellAbbrs = {
      nors = "sudo nixos-rebuild switch --flake /home/${username}/dotfiles#${osConfig.ap.core.hostname} -v --show-trace --log-format internal-json &| nom --json";
    };

    shellInit = ''
      # Disable greeting
      set -U fish_greeting
    '';
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
}
