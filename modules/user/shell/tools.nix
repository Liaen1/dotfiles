{pkgs, ...}: {
  home.packages = with pkgs; [
    file
    tldr
    unzip

    # disk
    duf
    du-dust
    fd
    ripgrep

    # network
    nethogs

    # hardware
    lm_sensors
    pciutils
    usbutils

    # media
    ffmpeg

    # nix
    deadnix
    nix-diff
    nix-du
    nix-melt
    nix-output-monitor
    nix-tree
    nixfmt
    nvd
    statix

    # useless stuff
    cmatrix
    cowsay
    fortune
    lolcat
    neofetch
    onefetch
    sl
    toilet
  ];

  programs = {
    bat = {
      enable = true;
      extraPackages = with pkgs.bat-extras; [
        batdiff
        batman
        batgrep
        batwatch
      ];
    };

    git = {
      enable = true;
      package = pkgs.gitAndTools.gitFull;
      userName = "Aatos Piha";
      userEmail = "aat.pih@protonmail.com";
      extraConfig.commit.verbose = true;
      delta.enable = true;
    };

    fzf.enable = true;

    lsd = {
      enable = true;
      enableAliases = true;
    };

    nix-index = {
      enable = true;
      enableFishIntegration = true;
    };

    nix-index-database.comma.enable = true;

    vim.enable = true;
  };
}
