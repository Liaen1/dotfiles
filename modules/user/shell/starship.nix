{
  imports = [./icons.nix];

  programs.starship = {
    enable = true;
    settings = {
      right_format = "$cmd_duration $time";

      cmd_duration = {
        show_milliseconds = true;
        format = "[$duration]($style) ";
        style = "bright-black";
      };

      directory = {
        style = "bold blue";
        truncation_symbol = ".../";
      };

      hostname = {disabled = true;};

      time = {
        format = "[$time]($style) ";
        style = "bright-black";
        disabled = false;
      };

      username.disabled = true;
    };
  };
}
