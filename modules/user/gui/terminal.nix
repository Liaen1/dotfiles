{
  programs.kitty = {
    enable = true;
    shellIntegration.enableFishIntegration = true;
    settings.window_padding_width = 2;
  };
}
