{
  programs.swaylock = {
    enable = true;
    settings = {
      font = "monospace";
      font-size = 18;
    };
  };
}
