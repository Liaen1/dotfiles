{
  lib,
  config,
  ...
}: {
  imports = [./boot.nix ./hardware-configuration.nix];

  system.stateVersion = "23.11";
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

  networking = {
    hostName = config.ap.core.hostname;
    hostId = "14ee0d61";
    useDHCP = lib.mkDefault true;
  };
}
